@echo off
setlocal
:PROMPT
SET /P AREYOUSURE=Are you sure to create a new KADMOS distribution (y/n)?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO END

sphinx-build doc_source doc
python setup.py sdist
python setup.py bdist_wheel

:END
endlocal

pause