from kadmos.graph import load

name_list = [('SellarARCG_01.kdms', 'Sellar problem - tool database'),
             ('SellarBFPGIDF_02.kdms', 'Sellar problem - MDO problem IDF'),
             ('SellarCMDAOIDF_03.kdms', 'Sellar problem - MDO strategy IDF'),
             ('SellarCMDAOIDF_03_mpg.kdms', 'Sellar problem - MDO strategy IDF'),
             ('SellarDFPGMDFGS_04.kdms', 'Sellar problem - MDO problem MDF'),
             ('SellarEMDAOMDFGS_05.kdms', 'Sellar problem - MDO strategy MDF'),
             ('SellarEMDAOMDFGS_05_mpg.kdms', 'Sellar problem - MDO strategy MDF'),
             ('SsbjARCG_06.kdms', 'SSBJ problem - tool database'),
             ('SsbjBFPG_07.kdms', 'SSBJ problem - MDO problem'),
             ('SsbjCBLISS_08.kdms', 'SSBJ problem - MDO strategy BLISS-2000'),
             ('SsbjCBLISS_08_mpg.kdms', 'SSBJ problem - MDO strategy BLISS-2000'),
             ('SsbjDCO_09.kdms', 'SSBJ problem - MDO strategy Collaborative Optimization'),
             ('SsbjDCO_09_mpg.kdms', 'SSBJ problem - MDO strategy Collaborative Optimization'),
             ('TudelftRCG_10.kdms', 'TU Delft wing design - tool database')]

for name in name_list:

    graph = load(name[0])
    graph.name = name[1]
    graph.save(name[0])