@echo off
setlocal
:PROMPT
SET /P AREYOUSURE=Are you sure to upload KADMOS to PyPi (y/n)?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO END

sphinx-build doc_source doc
python setup.py sdist upload -r pypi

:END
endlocal

pause