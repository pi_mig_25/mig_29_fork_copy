.. KADMOS documentation master file, created by
   sphinx-quickstart on Mon Jun 13 13:12:24 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to KADMOS's documentation!
==================================

See sidebar on the right for the table of contents.

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Documentation for the Code
==========================

.. automodule:: kadmos

Below the simplified class diagram of the KADMOS package is shown (not all methods are included for clarity). The UML can als be downloaded as PDF :download:`here <uml/KADMOS_UML.pdf>`.

.. image:: ../doc_source/uml/KADMOS_UML.png
   :width: 90 %
   :align: center

Example scripts for using KADMOS are available in the examples/scripts folder. The following scripts are available there:

* ``Sellar Problem``: Small two-disciplinary analytical MDO problem based on MDO literature.
* ``Super-sonic Business Jet``: The jet design is decribed at http://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/19980234657.pdf and was implemented in Python for OpenMDAO by Sylvain Dubreuil and Remi Lafage of ONERA, the French Aerospace Lab.
* ``TU Delft Wing Design``: The wing design case described here was developed at TU Delft and takes a variety of different disciplines into account.

Graph
*****

Below the KADMOS KadmosGraph class and its subclasses are listed.

KadmosGraph
~~~~~~~~~~~
.. autoclass:: kadmos.graph.graph_kadmos.KadmosGraph
   :members:

DataGraph
~~~~~~~~~
.. autoclass:: kadmos.graph.graph_data.DataGraph
   :members:

RepositoryConnectivityGraph
~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. autoclass:: kadmos.graph.graph_data.RepositoryConnectivityGraph
   :members:

FundamentalProblemGraph
~~~~~~~~~~~~~~~~~~~~~~~
.. autoclass:: kadmos.graph.graph_data.FundamentalProblemGraph
   :members:

MdaoProcessGraph
~~~~~~~~~~~~~~~~
.. autoclass:: kadmos.graph.graph_process.MdaoProcessGraph
   :members:

MdaoMixin
~~~~~~~~~
.. autoclass:: kadmos.graph.mixin_mdao.MdaoMixin
   :members:

EquationMixin
~~~~~~~~~~~~~
.. autoclass:: kadmos.graph.mixin_equation.EquationMixin
   :members:

VistomsMixin
~~~~~~~~~~~~
.. autoclass:: kadmos.graph.mixin_vistoms.VistomsMixin
   :members:

CMDOWS
******

Below the CMDOWS file operations library is listed.

.. autoclass:: kadmos.cmdows.cmdows.CMDOWS
   :members:

Utilities
*********

Below the utilities used by KADMOS are listed.

General
~~~~~~~~~~~~
.. automodule:: kadmos.utilities.general
   :members:

XML
~~~
.. automodule:: kadmos.utilities.xml
   :members:

Strings
~~~~~~~
.. automodule:: kadmos.utilities.strings
   :members:

Printing
~~~~~~~~
.. automodule:: kadmos.utilities.printing
   :members:

KnowledgeBase
*************

!! N.B. This class will be deprecated in future versions. !!

Below the full KADMOS KnowledgeBase class description can be found.

.. autoclass:: kadmos.knowledgebase.knowledgebase.KnowledgeBase
   :members: